from spyne.application import Application
from spyne.decorator import rpc
from spyne.service import ServiceBase
from spyne.server.wsgi import WsgiApplication
from spyne.protocol.soap import Soap11
from spyne.model.complex import Iterable
from spyne import Unicode
from wsgiref.simple_server import make_server
from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter
from geographiclib.geodesic import Geodesic
import vars
import googlemaps

#pip install spyne & lxml
#pip install zeep
#pip install geopy & Nominatim & geographiclib




class SoapService(ServiceBase):
    @rpc(Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, _returns=Iterable(Unicode))
    def get_point_trip(ctx, pourcentage, nombre_recharge, autonomy, gps_source, gps_destination, coord_type):
        #Define the ellipsoid
        geolocator = Nominatim(user_agent="application")
        
        geocode = RateLimiter(geolocator.geocode, min_delay_seconds=1, max_retries=0)
        location_source = geocode(gps_source)
        location_destination = geocode(gps_destination)

        geod = Geodesic.WGS84
        inv = geod.Inverse(float(location_source.latitude), float(location_source.longitude), float(location_destination.latitude), float(location_destination.longitude))
        azi1 = inv['azi1']
        
        s = int(autonomy)*750

        all_points = []

        # Calcul du premier point : 
        first_point = int((int(autonomy)*int(pourcentage)*10) - (int(autonomy)*250))

        coord = geod.Direct(float(location_source.latitude), float(location_source.longitude), azi1, first_point)
        if coord_type == "lat":
            all_points.append(str(coord['lat2']))
        elif coord_type == "lon":
            all_points.append(str(coord['lon2']))

        # Calcul des autres points si nécessaire :
        if int(nombre_recharge) > 1:
            for i in range(int(nombre_recharge)-1):
                coord = geod.Direct(float(location_source.latitude), float(location_source.longitude), azi1, (i*s)+first_point)
                if coord_type == "lat":
                    all_points.append(str(coord['lat2']))
                elif coord_type == "lon":
                    all_points.append(str(coord['lon2']))
            
        return all_points


    @rpc(Unicode, Unicode, _returns=Iterable(Unicode))
    def get_trip_info(ctx, city_source, city_destination):
        # On se créer un client googleMaps avec notre clé API
        gmaps = googlemaps.Client(key=vars.GOOGLE_API_KEY)

        # On effectue une requête pour obetnir la direction 
        direction_result = gmaps.directions(city_source, city_destination)

        distance = ''
        duree = ''
        start_location = ''
        end_location = ''

        for data in direction_result:
            distance = data['legs'][0]['distance']['value']
            duree = data['legs'][0]['duration']['value']
            start_location = data['legs'][0]['start_address']
            end_location = data['legs'][0]['end_address']

        return str(distance), str(duree), start_location, end_location

    
application = Application([SoapService], 'spyne.examples.hello.soap',
in_protocol=Soap11(validator='lxml'),
out_protocol=Soap11())
wsgi_application = WsgiApplication(application)

# sur google : 127.0.0.1:8000/?wsdl
server = make_server('127.0.0.1', 8092, wsgi_application)
server.serve_forever()