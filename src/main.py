from flask import Flask, render_template, request
from zeep import Client
from flask_mysqldb import MySQL
from math import floor, ceil
import vars
import requests


app = Flask(__name__)
app.config['MYSQL_HOST'] = vars.MYSQL_HOST
app.config['MYSQL_USER'] = vars.MYSQL_USER
app.config['MYSQL_PASSWORD'] = vars.MYSQL_PASSWORD
app.config['MYSQL_DB'] = vars.MYSQL_DB

mysql = MySQL(app)


# TO DO LIST :
# - Calcul du temps total avec les temps d'arrêts (rechargement)                           | OK
# - Recherche de bornes en fonction des steps retourner par l'API. (On est des cracks)     | --
# - Recherche à 3Km, puis 6Km, puis 9...                                                   | OK
# - Check si la voiture peut accéder à la destination si entre 75% et 100%.                | OK
# - CSS                                                                                    | OK


def get_bornes(lat, lon):
    check = True
    dist = 3000 #Distance (m)

    while check:
        URL = f"https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=bornes-irve&q=&rows=30&geofilter.distance={lat}%2C+{lon}%2C+{int(dist)}"
        dist = int(dist)+3000
        response = requests.get(URL)
        api_data = response.json()

        if api_data['nhits'] != 0:
            check = False

    return api_data['records'][0]['fields']['ylatitude'] , api_data['records'][0]['fields']['xlongitude']
            
        



@app.route("/", methods=['GET', 'POST'])
def index():

    cur = mysql.connection.cursor()
    cur.execute("SELECT model, id_car FROM cars")
    
    cars= cur.fetchall()
    
    listCar = {}
    for c in cars:
        listCar[c[1]] = c[0]

    cur.close()

    if request.method == 'POST':
        if request.form.get('action1') == 'Calculer':
            car_id = request.form.get('car_id')
            pourcentage = request.form.get('pourcentage')
            city_source = request.form['source']
            city_destination = request.form['destination']

            cur = mysql.connection.cursor()
            cur.execute(f"SELECT model, autonomy, recharge_time FROM cars WHERE id_car={car_id};")
            car_info= cur.fetchall()
            
            for info in car_info:
                model = info[0]
                autonomy = int(info[1])
                recharge_time = info[2]

            client = Client('http://127.0.0.1:8092?wsdl')

            distance, duree, start_location, end_location = client.service.get_trip_info(city_source, city_destination)

            nombre_recharge = 0
            formated_waypoints = []
            if (autonomy*1000) < int(distance):
                
                nombre_recharge = floor(int(distance)/(autonomy*750)) # recharge à 25%
                
                # Peut on atteindre la fin avec 100 d'autonomie            
                if ((autonomy*0.75)*(nombre_recharge-1))+autonomy > int(distance)/1000:
                    nombre_recharge = nombre_recharge - 1

                list_lat = client.service.get_point_trip(pourcentage, nombre_recharge+1, autonomy, city_source, city_destination, 'lat')
                list_lon = client.service.get_point_trip(pourcentage, nombre_recharge+1, autonomy, city_source, city_destination, 'lon')

                # A cause de geopy - 1ere donnée useless
                list_lat = list_lat[1:]
                list_lon = list_lon[1:]
                waypoints=[]
                for (index, coord) in enumerate(list_lat):
                    lat, lon = get_bornes(coord,list_lon[index])
                    waypoints.append("via:{},{}".format(lat,lon))
                
                for waypoint in waypoints:
                    first, second = waypoint.split(",")
                    poubelle, first = first.split(":")
                    formated_waypoints.append({"location":f"{first},{second}"})

                # On ajoute le temps de recharge du véhicule
                temps_recharge_total = nombre_recharge*recharge_time*60
                duree = int(duree) + temps_recharge_total

            # Calcul du temps
            heures = floor(int(duree)/3600)
            minutes_pourcentage = (int(duree)/3600)%1
            minutes = minutes_pourcentage*60

            if heures == 0:
                duree = "{} minute(s)".format(ceil(minutes))
            elif heures > 0:
                duree = "{} heure(s) {} minute(s)".format(heures,ceil(minutes))

            # Affichage plus propre de la distance
            if (int(distance)/1000) >= 2:
                embed_dist = "{} kilomètres".format(ceil(int(distance)/1000))
            elif 2 > (int(distance)/1000) >= 1:
                embed_dist = "{} kilomètre".format(ceil(int(distance)/1000))
            else:
                embed_dist = "{} mètres".format(ceil(int(distance)))
            
            return render_template('index.html', cars=listCar, distance=embed_dist, duree=duree, start_location=start_location, end_location=end_location, model=model, autonomy=autonomy, recharge_time=recharge_time, nbr_recharge=nombre_recharge, waypoints=str(formated_waypoints))

    return render_template("index.html", cars=listCar)

# app.run("127.0.0.1", debug=True, port=5000)
app.run("188.34.202.209", debug=True, port=5000)
